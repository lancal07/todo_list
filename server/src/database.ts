import mongoose from 'mongoose'

async function Connect(): Promise <void> {
    try{
        await mongoose
        .connect('mongodb://mongo:27017/todo_list',{
	  useNewUrlParser: true,
	  useUnifiedTopology: true,
	  useFindAndModify: false,
          useCreateIndex: true
	})
        .then(() => {
            console.log('>>> Database Connected :) ')
        });
    }
    catch(error:any){
        console.log(`Connection Failed!: ${error}`)
        return process.exit(1)
    }

}

export default Connect;
