import mongoose from 'mongoose'
import bcrypt from 'bcrypt'

export interface User extends mongoose.Document {

  name: String
  username: String
  password: String
  encryptPassword(password:String): String
  matchPassword(password:String): Boolean

}

const UserSchema = new mongoose.Schema({

  name: {type:String, required:true},
  username: {type:String, required:true},
  password: {type:String,required:true}

})

UserSchema.methods.encryptPassword = async (password:string): Promise <string> => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

UserSchema.methods.matchPassword = async function (password:string) {
  return await bcrypt.compare(password, this.password);
};

export default mongoose.model<User>('User',UserSchema);
