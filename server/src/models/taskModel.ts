import mongoose from 'mongoose'

export interface Task extends mongoose.Document {

  name: String
  description: String
  state: Boolean
  user: String

}

const TaskSchema = new mongoose.Schema({

  name: {type:String,required: true},
  description: {type:String, required:true},
  state: {type:Boolean,required:true,default:false},
  user: {type:String,required:true}

})

export default mongoose.model<Task>('Task',TaskSchema);
