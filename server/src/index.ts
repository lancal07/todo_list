import Server from './app';
import database from './database';

database(); 

const server = new Server();
server.start();
