import {Router} from 'express';

import {taskController} from '../controllers/taskController';

class TaskRoutes {

    public router: Router = Router();

    constructor() {

        this.config();

    }

    config(): void{

      this.router.get('/task/:id',taskController.getTask);
      this.router.post('/add/task/:id',taskController.addTask);
      this.router.patch('/update/task/:id',taskController.updateTask);

    }

}

const taskRoutes = new TaskRoutes();
export default taskRoutes.router;
