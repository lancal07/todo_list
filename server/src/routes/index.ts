import {Router} from 'express';

import taskRoutes from './taskRoutes';
import userRoutes from './userRoutes';

class IndexRoutes {

  public router: Router = Router();

    //constructor
    constructor() {

        this.config();
    }

    //configure
    config (): void {

        this.router.use(taskRoutes);
        this.router.use(userRoutes);

    }

}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router;
