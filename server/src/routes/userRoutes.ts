import {Router} from 'express';

import {userController} from '../controllers/userController';

class UserRoutes {

  public router: Router = Router();

  constructor() {
  
    this.config();
  
  }

  config(): void {

    this.router.post('/users/sigin',userController.getUsers);
    this.router.post('/users/sigup',userController.createUser);
  }

}

const userRoutes = new UserRoutes();
export default userRoutes.router;
