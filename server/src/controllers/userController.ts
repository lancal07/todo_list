import {Request,Response,NextFunction} from 'express';

import User from '../models/userModel';

class UserController {
  
  public async createUser(req: Request, res: Response, next: NextFunction): Promise <any> {
  
    try {

      const errors = [];
      const {name,username,password,confirmPassword} = req.body;
      if(password != confirmPassword){
      
	errors.push({text: 'Password do not match :('});
      
      }
      if ( password.length < 4){
      
	errors.push({text: 'Password must be at least 4 characters.'});
      }

      if(errors.length > 0) {
      
	res.status(400).json(errors);
	
 
      }else{
      
	//res.send('sigup successful');
	const userName = await User.findOne({username:username});
	if(userName){
	
	  errors.push({text: 'User has ready created, please choose another one'});
	  res.status(400).json(errors);
	  console.log('User has ready created ', userName); 
        }else{
	
	  // Saving a New User
      	  const newUser = new User({ name, username, password });
          newUser.password = await newUser.encryptPassword(password);
      	  await newUser.save();
	  res.status(200).json('User Created :)');
	
	}	

      }
      console.log(req.body);
      
    }catch(error){
     
      
      return next(error)
    }
  
  }
  
  public async getUsers(req: Request, res: Response, next: NextFunction): Promise <any> {
  
    const errorsUser = [];

    try {
     
     const{username,password} = req.body; 
     const userName = await User.findOne({username:username});
     if(!userName){
     	
       errorsUser.push({text: 'User is not Registered'});
       res.status(400).json(errorsUser);
     }else{
     
       const match = await userName.matchPassword(password);
       if(match){
       
	 return res.status(200).json({'Ok':'Passed'});
       }else{
       
	 errorsUser.push({text: 'Invalid Password'});
	 return res.status(400).json(errorsUser);
       }
     }
     console.log(req.body)
    
    }catch(error){
      
      return next(error)
    }
  
    res.send('getUsers');
  
  }
  
}

export const userController = new UserController();
