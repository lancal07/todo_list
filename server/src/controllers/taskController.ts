import {Request, Response, NextFunction} from 'express'
import Task from '../models/taskModel'

class TaskController {

  public async getTask(req: Request, res: Response, next: NextFunction): Promise <any> {
  
    try{
    
      const errorsTask = [];
      const id = req.params.id;
      const task = await Task.find({user:id});
      console.log(req);
      if(task){
      
	res.status(200).json(task);
      
      }else{
      
	errorsTask.push({text: 'User without task'});
	res.status(400).json(errorsTask);
	
      }
    
    }catch(error){
    
      return next(error)
    
    }

  }

  public async addTask (req: Request, res: Response, next: NextFunction): Promise <any> {

    try{
      
      const id = req.params.id;
      const {name, description} = req.body;
      const newTask = new Task({name,description});
      newTask.user = id;
      console.log(newTask);
      res.send('Task Created :)');
      return await newTask.save();
    }catch(error){
    
      return next(error)
    }
  }

public async updateTask (req: Request, res: Response, next: NextFunction): Promise <any> {

    try{
      
      const id = req.params.id;
      const taskState = await Task.find({user:id});
      console.log('taskState: ',);
      if(taskState){
	res.status(200).json(taskState);
      }
    }catch(error){
    
      return next(error)
    }
  }

}

export const taskController = new TaskController();
