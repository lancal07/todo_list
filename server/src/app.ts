import express, {Application} from 'express';
import morgan from 'morgan';
import cors from 'cors';
import dotenv from 'dotenv';
import routes from './routes/index';

class Server {

    public app: Application;

    // constructor
    constructor() {

        this.app = express();
        this.config();
        this.middleware();
        this.routes();
    }

     // Middlewares
    middleware() {
        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));
    }

    // settings
    config(): void {

        dotenv.config()
        this.app.set('port',process.env.PORT || 3000);
        this.app.use(cors());
    }

    // Routes
    routes(): void {

  	this.app.use('/',routes);

    }

   // Starting Server
    async start(): Promise <void> {

        this.app.listen(this.app.get('port'), () => {
             console.log('Server on Port ', this.app.get('port'));
         });

    }
}

export default Server;
