"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const taskRoutes_1 = __importDefault(require("./taskRoutes"));
const userRoutes_1 = __importDefault(require("./userRoutes"));
class IndexRoutes {
    //constructor
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    //configure
    config() {
        this.router.use(taskRoutes_1.default);
        this.router.use(userRoutes_1.default);
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
//# sourceMappingURL=index.js.map