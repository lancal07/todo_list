"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const taskController_1 = require("../controllers/taskController");
class TaskRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/task/:id', taskController_1.taskController.getTask);
        this.router.post('/add/task/:id', taskController_1.taskController.addTask);
        this.router.patch('/update/task/:id', taskController_1.taskController.updateTask);
    }
}
const taskRoutes = new TaskRoutes();
exports.default = taskRoutes.router;
//# sourceMappingURL=taskRoutes.js.map