"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.taskController = void 0;
const taskModel_1 = __importDefault(require("../models/taskModel"));
class TaskController {
    getTask(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errorsTask = [];
                const id = req.params.id;
                const task = yield taskModel_1.default.find({ user: id });
                console.log(req);
                if (task) {
                    res.status(200).json(task);
                }
                else {
                    errorsTask.push({ text: 'User without task' });
                    res.status(400).json(errorsTask);
                }
            }
            catch (error) {
                return next(error);
            }
        });
    }
    addTask(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const { name, description } = req.body;
                const newTask = new taskModel_1.default({ name, description });
                newTask.user = id;
                console.log(newTask);
                res.send('Task Created :)');
                return yield newTask.save();
            }
            catch (error) {
                return next(error);
            }
        });
    }
    updateTask(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const taskState = yield taskModel_1.default.find({ user: id });
                taskState.state = true;
                if (taskState) {
                    res.status(200).json(taskState);
                }
            }
            catch (error) {
                return next(error);
            }
        });
    }
}
exports.taskController = new TaskController();
//# sourceMappingURL=taskController.js.map