"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userController = void 0;
const userModel_1 = __importDefault(require("../models/userModel"));
class UserController {
    createUser(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const errors = [];
                const { name, username, password, confirmPassword } = req.body;
                if (password != confirmPassword) {
                    errors.push({ text: 'Password do not match :(' });
                }
                if (password.length < 4) {
                    errors.push({ text: 'Password must be at least 4 characters.' });
                }
                if (errors.length > 0) {
                    res.status(400).json(errors);
                }
                else {
                    //res.send('sigup successful');
                    const userName = yield userModel_1.default.findOne({ username: username });
                    if (userName) {
                        errors.push({ text: 'User has ready created, please choose another one' });
                        res.status(400).json(errors);
                        console.log('User has ready created ', userName);
                    }
                    else {
                        // Saving a New User
                        const newUser = new userModel_1.default({ name, username, password });
                        newUser.password = yield newUser.encryptPassword(password);
                        yield newUser.save();
                        res.status(200).json('User Created :)');
                    }
                }
                console.log(req.body);
            }
            catch (error) {
                return next(error);
            }
        });
    }
    getUsers(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const errorsUser = [];
            try {
                const { username, password } = req.body;
                const userName = yield userModel_1.default.findOne({ username: username });
                if (!userName) {
                    errorsUser.push({ text: 'User is not Registered' });
                    res.status(400).json(errorsUser);
                }
                else {
                    const match = yield userName.matchPassword(password);
                    if (match) {
                        return res.status(200).json({ 'Ok': 'Passed' });
                    }
                    else {
                        errorsUser.push({ text: 'Invalid Password' });
                        return res.status(400).json(errorsUser);
                    }
                }
                console.log(req.body);
            }
            catch (error) {
                return next(error);
            }
            res.send('getUsers');
        });
    }
}
exports.userController = new UserController();
//# sourceMappingURL=userController.js.map