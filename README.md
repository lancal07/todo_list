# TODO-list

In order to run this project, follow these steps:

1. Clone the repository ```git clone https://gitlab.com/lancal07/todo_list.git```
2. Enter the main directory ```cd TODO-list```
3. Mount the project with docker composer ```docker-compose up```
4. Have Fun! :)

## Ports

- Client: http://localhost:4200/
- Server: http://localhost:3000/
